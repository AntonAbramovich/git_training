class Color:
    def __init_light__(component):
        if type(component) is not int or component < 0:
            component = 0
        elif component > 255:
            component = 255

        return component

    def __init__(self, r, g, b):
        self.R = Color.__init_light__(r)
        self.G = Color.__init_light__(g)
        self.B = Color.__init_light__(b)

    def get_R(self):
        return self.R

    def get_G(self):
        return self.G

    def get_B(self):
        return self.B

    def __repr__(self):
        return f"R={self.R} G={self.G} B={self.B}"

    def brightness(self):
        return 0.299 * self.R + 0.587 * self.G + 0.114 * self.B

    def is_compatible(self, other):
        return abs(self.brightness() - other.brightness()) > 128